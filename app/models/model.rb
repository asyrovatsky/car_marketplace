# == Schema Information
#
# Table name: models
#
#  id          :bigint           not null, primary key
#  brand_id    :integer
#  parent_id   :integer
#  name        :string
#  cars_count  :integer
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Model < ApplicationRecord
  belongs_to :parent,
    class_name: 'Model',
    optional: true,
    inverse_of: :sub_models

  has_many :sub_models,
    class_name: 'Model',
    foreign_key: 'parent_id',
    inverse_of: :parent

  belongs_to :brand

  def brand_with_model
    "#{brand.name} #{name}"
  end

  scope :prime_models, -> {where("models.parent_id is null")}
end
