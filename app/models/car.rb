# == Schema Information
#
# Table name: cars
#
#  id         :bigint           not null, primary key
#  name       :string
#  fueltype   :string
#  bodytype   :string
#  gearbox    :string
#  price      :float
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Car < ApplicationRecord
end
