require 'rails_helper'

RSpec.describe "cars/show", type: :view do
  before(:each) do
    assign(:car, Car.create!(
      name: "Name",
      fueltype: "Fueltype",
      bodytype: "Bodytype",
      gearbox: "Gearbox",
      price: 2.5
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Fueltype/)
    expect(rendered).to match(/Bodytype/)
    expect(rendered).to match(/Gearbox/)
    expect(rendered).to match(/2.5/)
  end
end
