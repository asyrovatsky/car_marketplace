require 'rails_helper'

RSpec.describe "cars/new", type: :view do
  before(:each) do
    assign(:car, Car.new(
      name: "MyString",
      fueltype: "MyString",
      bodytype: "MyString",
      gearbox: "MyString",
      price: 1.5
    ))
  end

  it "renders new car form" do
    render

    assert_select "form[action=?][method=?]", cars_path, "post" do

      assert_select "input[name=?]", "car[name]"

      assert_select "input[name=?]", "car[fueltype]"

      assert_select "input[name=?]", "car[bodytype]"

      assert_select "input[name=?]", "car[gearbox]"

      assert_select "input[name=?]", "car[price]"
    end
  end
end
