require 'rails_helper'

RSpec.describe "cars/index", type: :view do
  before(:each) do
    assign(:cars, [
      Car.create!(
        name: "Name",
        fueltype: "Fueltype",
        bodytype: "Bodytype",
        gearbox: "Gearbox",
        price: 2.5
      ),
      Car.create!(
        name: "Name",
        fueltype: "Fueltype",
        bodytype: "Bodytype",
        gearbox: "Gearbox",
        price: 2.5
      )
    ])
  end

  it "renders a list of cars" do
    render
    cell_selector = Rails::VERSION::STRING >= '7' ? 'div>p' : 'tr>td'
    assert_select cell_selector, text: Regexp.new("Name".to_s), count: 2
    assert_select cell_selector, text: Regexp.new("Fueltype".to_s), count: 2
    assert_select cell_selector, text: Regexp.new("Bodytype".to_s), count: 2
    assert_select cell_selector, text: Regexp.new("Gearbox".to_s), count: 2
    assert_select cell_selector, text: Regexp.new(2.5.to_s), count: 2
  end
end
