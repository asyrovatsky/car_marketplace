require 'rails_helper'

RSpec.describe "cars/edit", type: :view do
  let(:car) {
    Car.create!(
      name: "MyString",
      fueltype: "MyString",
      bodytype: "MyString",
      gearbox: "MyString",
      price: 1.5
    )
  }

  before(:each) do
    assign(:car, car)
  end

  it "renders the edit car form" do
    render

    assert_select "form[action=?][method=?]", car_path(car), "post" do

      assert_select "input[name=?]", "car[name]"

      assert_select "input[name=?]", "car[fueltype]"

      assert_select "input[name=?]", "car[bodytype]"

      assert_select "input[name=?]", "car[gearbox]"

      assert_select "input[name=?]", "car[price]"
    end
  end
end
