require 'rails_helper'

RSpec.describe "models/edit", type: :view do
  let(:model) {
    Model.create!(
      name: "MyString",
      parent_id: 1,
      cars_count: 1,
      description: "MyString"
    )
  }

  before(:each) do
    assign(:model, model)
  end

  it "renders the edit model form" do
    render

    assert_select "form[action=?][method=?]", model_path(model), "post" do

      assert_select "input[name=?]", "model[name]"

      assert_select "input[name=?]", "model[parent_id]"

      assert_select "input[name=?]", "model[cars_count]"

      assert_select "input[name=?]", "model[description]"
    end
  end
end
