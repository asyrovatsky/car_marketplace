require 'rails_helper'

RSpec.describe "models/index", type: :view do
  before(:each) do
    assign(:models, [
      Model.create!(
        name: "Name",
        parent_id: 2,
        cars_count: 3,
        description: "Description"
      ),
      Model.create!(
        name: "Name",
        parent_id: 2,
        cars_count: 3,
        description: "Description"
      )
    ])
  end

  it "renders a list of models" do
    render
    cell_selector = Rails::VERSION::STRING >= '7' ? 'div>p' : 'tr>td'
    assert_select cell_selector, text: Regexp.new("Name".to_s), count: 2
    assert_select cell_selector, text: Regexp.new(2.to_s), count: 2
    assert_select cell_selector, text: Regexp.new(3.to_s), count: 2
    assert_select cell_selector, text: Regexp.new("Description".to_s), count: 2
  end
end
