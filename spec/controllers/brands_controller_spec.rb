require 'rails_helper'

RSpec.describe BrandsController, type: :controller do
  describe "GET index" do
    let(:brand1) { create(:brand, name: 'Audi') }
    let(:brand2) { create(:brand, name: 'BMW') } 

    it 'assigns @brands' do
      get :index
      expect(assigns(:brands)).to eq([brand1, brand2])
    end
    
    it "has a 200 status code" do
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe 'GET show' do
    let(:brand) { create(:brand, name: 'Audi') }

    it 'assigns variables' do
      get :show, params: {id: brand.id}
      expect(assigns(:brand)).to eq(brand)
    end

    it "has a 200 status code" do
      get :show, params: {id: brand.id}
      expect(response.status).to eq(200)
    end
  end

  describe 'GET new' do
    let!(:brand) { create(:brand, name: 'Audi') }

    it 'assigns @brand' do
      get :new
      expect(assigns(:brand)).to be_a(Brand)
    end
    
    it "has a 200 status code" do
      get :new
      expect(response.status).to eq(200)
    end
  end

  describe 'POST create' do

    it 'creates brand' do
      expect(Brand.first).to be_nil
      post :create, params: {brand: {name: 'Audi'} } 
      expect(Brand.where(name: 'Audi').first).not_to be_nil
    end

    it "has a 302 status code" do
      post :create, params: {brand: {name: 'Audi'} } 
      expect(response.status).to eq(302)
    end
  end

  describe 'PUT update' do  
    let!(:brand) { create(:brand, name: 'BMW') }

    it 'updates movie' do
      patch :update, params: {id: brand.id, brand: {name: 'Audi'}} 
      expect(Brand.first.name).not_to eq(brand.name)
    end
  end

  describe 'DELETE destroy' do
    let!(:brand) { create(:brand, name: 'Horror') }

    it 'deletes movie' do
      delete :destroy, params: {id: brand.id} 
      expect(Brand.first).to be_nil
    end
  end
end
