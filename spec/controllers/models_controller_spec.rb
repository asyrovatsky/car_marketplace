require 'rails_helper'

RSpec.describe ModelsController, type: :controller do
  describe "GET index" do
    let(:model1) { create(:model, brand: brand1, name: 'C300') }
    let(:model2) { create(:model, brand: brand2,name: 'S300') } 
    let(:brand1) { create(:brand, name: 'Audi') }
    let(:brand2) { create(:brand, name: 'BMW') }
    

    it 'assigns @models' do
      get :index
      expect(assigns(:models)).to eq([model1, model2])
    end
    
    it "has a 200 status code" do
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe 'GET show' do
    let!(:model) { create(:model, brand: brand, name: 'C300') }
    let!(:brand) { create(:brand, name: 'Audi') }

    it 'assigns variables' do
      get :show, params: {id: model.id, brand_id: brand.id}
      expect(assigns(:model)).to eq(model)
    end

    it "has a 200 status code" do
      get :show, params: {id: model.id, brand_id: brand.id}
      expect(response.status).to eq(200)
    end
  end

  describe 'GET new' do
    let!(:brand) { create(:brand, name: 'Audi') }

    it 'assigns @model' do
      get :new
      expect(assigns(:model)).to be_a(Model)
    end
    
    it "has a 200 status code" do
      get :new
      expect(response.status).to eq(200)
    end
  end

  describe 'POST create' do
    let!(:brand) { create(:brand, name: 'Audi') }

    it 'creates model' do
      expect(Model.first).to be_nil
      post :create, params: {model: {name: 'Audi', brand_id: brand.id} } 
      expect(Model.where(name: 'Audi').first).not_to be_nil
    end

    it "has a 302 status code" do
      post :create, params: {model: {name: 'Audi', brand_id: brand.id} } 
      expect(response.status).to eq(302)
    end
  end

  describe 'PUT update' do  
    let!(:model) { create(:model,  brand: brand, name: 'C300') }
    let!(:brand) { create(:brand, name: 'Audi') }

    it 'updates movie' do
      patch :update, params: {id: model.id, model: {name: 'Audi', brand_id: brand.id}} 
      expect(Model.first.name).not_to eq(model.name)
    end
  end

  describe 'DELETE destroy' do
    let!(:model) { create(:model, brand: brand, name: 'C300') }
    let!(:brand) { create(:brand, name: 'Audi') }

    it 'deletes model' do
      delete :destroy, params: {id: model.id} 
      expect(Model.first).to be_nil
    end
  end
end