# == Schema Information
#
# Table name: brands
#
#  id          :bigint           not null, primary key
#  name        :string
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
require 'rails_helper'

RSpec.describe Brand, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:models) }
  end
end

