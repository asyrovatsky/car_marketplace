# == Route Map
#

Rails.application.routes.draw do
  resources :models
  resources :dealers
  resources :brands
  resources :cars
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
